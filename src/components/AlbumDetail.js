import React from 'react';
import { View, Text, Image, Linking } from 'react-native';

import Card from './Card';
import CardSection from './CardSection';
import Button from './Button';

const AlbumDetail = (props) => {
    const { title, artist, image, thumbnail_image, url } = props.album;
    const {
      headerContentStyle,
      textStyle,
      imageStyle,
      imageContainerStyle,
      coverStyle
    } = styles;

    return (
      <Card>
        <CardSection>
          <View style={imageContainerStyle}>
            <Image
              source={{ uri: thumbnail_image }}
              style={imageStyle}
            />
          </View>
          <View style={headerContentStyle}>
            <Text style={textStyle}>{title}</Text>
            <Text >{artist}</Text>
          </View>
        </CardSection>

        <CardSection>
          <Image
            source={{ uri: image }}
            style={coverStyle}
          />
        </CardSection>

        <CardSection>
          <Button onPress= {() => Linking.openURL(url)}>
            Buy Now!
          </Button>
        </CardSection>

      </Card>
    );
};

const styles = {
    headerContentStyle: {
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    textStyle: {
        fontSize: 18
    },
    imageStyle: {
        height: 50,
        width: 50
    },
    imageContainerStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10
    },
    coverStyle: {
        height: 300,
        flex: 1,
        width: null
    }
};

export default AlbumDetail;
