import React from 'react';
import { View, Text } from 'react-native';

const Header = (props) => {
    const { viewStyle, textStyle } = styles;

    return (
    <View style={viewStyle}>
      <Text style={textStyle}>{props.headerText}</Text>
    </View>
    );
};

const styles = {
    viewStyle: {
        backgroundColor: '#3b5998',
        justifyContent: 'center', //move half down of the view tag
        alignItems: 'center', //move half across the view tag
        height: 60,
        paddingTop: 10,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 10 }, // dimentions of the shadow
        shadowOpacity: 0.2, //shadow darkness
        elevation: 2,
        position: 'relative'

    },
    textStyle: {
        fontSize: 20
    }
};

export default Header;
